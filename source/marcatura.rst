﻿.. _MARCATURA:

#########
MARCATURA
#########
*****************
PRIMA DELLO START
*****************
I comandi principali di gestione del laser che si trovano nella barra comandi in alto sono (in ordine di disposizione sulla barra):

* :kbd:`Luce` (accende e spegne il faretto interno alla camera di lavoro e permette di ispezionare la camera anche durante la lavorazione);
* :kbd:`Rosso` (attiva la proiezione del puntatore rosso per visualizzare l'area di marcatura e permettere la centratura del pezzo);
* :kbd:`Avvia Marcatura` (è il tasto che abilita la marcatura);
* :kbd:`Ferma Operazioni` (ferma qualsiasi attività di marcatura in corso);
* :kbd:`Anteprima di Marcatura` (visualizza l'anteprima della marcatura);
* :kbd:`Stima Tempi` (premendo sull'icona il laser apre nel programma di editor una finestra per stimare i tempi di lavorazione).

.. _marcatura_comandi_laser:
.. figure:: _static/marcatura_comandi_laser.png
   :width: 14 cm
   :align: center

   Comandi di gestione laser

*********************
AVVIO MARCATURA LASER
*********************
#. Premere :kbd:`AVVIA ROSSO`;
#. Posizionare il materiale all'interno della camera, centrando il rosso sull'area interessata alla marcatura;
#. Chiudere il portellone del laser;
#. Premere :kbd:`AVVIA LASER`

Attendere che il laser termini la lavorazione prima di aprire lo sportello.

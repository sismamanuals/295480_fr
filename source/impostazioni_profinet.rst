.. _impostazioni_profinet:

##########################
IMPOSTAZIONI MENÙ PROFINET
##########################
Questo menù permette l'integrazione tra PRISMA e il protocollo Siemens Profinet. Lo scopo principale è la condivisione dei dati tramite il bus Profinet e il software PRISMA. Questa funzione è disponibile a partire dalla versione 1.14.6828 di PRISMA.

Il menù delle :guilabel:`IMPOSTAZIONI APPLICAZIONE > Profinet` si suddivide in finestre che, a loro volta, contengono ulteriori parametri.
Di seguito descrizione dei parametri.

.. NOTE::

   |notice| Per un corretto funzionamento del sistema è necessario impostare anche il PLC Siemens come descritto alla :numref:`impostazioni_PLC_Siemens`.


Il PLC Siemens può essere utilizzato anche per inviare comandi al software PRISMA. In questo caso PRISMA rimane in uno stato di ascolto e continua a leggere i valori in modalità polling con un intervallo di 1 s.

.. _impostazioni_applicazione_profinet:
.. figure:: _static/impostazioni_applicazione_profinet.png
   :width: 14 cm
   :align: center

   Finestra di Configurazione lettura Comandi

******************
PROPRIETÀ GENERALI
******************
**Abilita gestore progetti Profinet**

   Abilita la funzione e il thread di polling.

**Nome impianto**

   Nome dell'impianto, non viene utilizzato per la comunicazione.

**Projects folder**

   Percorso della cartella in cui si trova il progetto, nel PLC si può memorizzare solo il nome del progetto.

**IP**

   Indirizzo IP del PLC, tutti i dati devono essere memorizzati nello stesso dispositivo.

**Tipo CPU**

   Tipo di PLC (S7200, S7300, S7400, **S71200**, S71500).

**Rack [0]**

   Dipende dalla configurazione del PLC, se è un singolo dispositivo 0 è il valore predefinito.

**Slot [0]**

   Dipende dalla configurazione del PLC, se è un singolo dispositivo 0 è il valore predefinito.

*******************
PROPRIETÀ VARIABILI
*******************
Il sistema può accettare 4 oggetti di tipo *variabile*. Per ognuno di loro è possibile impostare:

**Abilita**

   Per attivare la variabile.

**DataBlock**

   Indirizzo dell'area di memoria in cui sono memorizzati i dati.

**Start byte address**

   Indirizzo del primo byte di dati.

**Data Type**

   Posizione dell'area di memoria (Input, Output, Memory, **DataBlock**, Timer, Counter).

**Variable type**

   Specifica il tipo di dati (Bit, Byte, Word, DWord, Int, Dint, Real, String, **StringEx**, Timer, Counter).

**Variable count**

   La lunghezza dei dati in byte. I dati delle stringhe del modello S71200 sono di 256 byte.

*****************
TIPI DI VARIABILI
*****************
#. **COMMANDS**: è usato per condividere il comando e la sintassi è:

   * **LOAD PROJECT**: carica il progetto definito nel campo ``PROJECT NAME``. Deve essere il nome del progetto e verrà cercato all'interno del percorso della cartella dei progetti;
   * **CLOSE PROJECT**: chiudi il progetto aperto corrente. Il sistema può accettare solo un progetto per volta, quindi tutto il progetto aperto verrà chiuso automaticamente se vi è un secondo comando **LOAD PROJECT**;
   * **START**: avvia il progetto e il laser si avvia;
   * **STOP**: invia un comando di stop a PRISMA e il progetto si arresta;

#. **PROJECT NAME**: memorizza il nome del progetto. Questo valore deve essere impostato 1 s prima di avviare il comando **LOAD PROJECT**;

#. **MACHINE STATUS** : contiene lo stato della macchina laser. Tutto il valore di stato potrebbe essere ritardato di 1 s a causa del tempo di polling. Gli stati disponibili sono:

   * **IDLE**: appare quando il sistema è pronto e nessun progetto è caricato;
   * **LOADED**: dopo aver caricato il progetto;
   * **MARKING**: quando il ciclo di marcatura è in corso, non è solo l'ora del laser;
   * **END**: al termine del ciclo di marcatura quando il sistema potrebbe accettare un altro comando **START**;
   * **ERROR**: quando il ciclo di marcatura va in errore;

#. **WATCHDOG**: *da descrivere*.

*****************
LETTURA VARIABILI
*****************

.. |type| image:: _static/impostazioni_profinet_logo_tipo.png
   :width: 2 cm
   :align: middle

L'operazione principale possibile in PRISMA è condividere le variabili e usarle come valori nei contatori.

* Creare una variabile e impostare il tipo in modalità *Profinet* |type|;
* Impostare un valore di **KEY** per chiamare questa connessione in un oggetto di testo (vedere :numref:`impostazioni_applicazione_profinet_variabili`).

.. _impostazioni_applicazione_profinet_variabili:
.. figure:: _static/impostazioni_applicazione_profinet_variabili.png
   :width: 14 cm
   :align: center

   Selezione Variabili


Dopo aver creato la variabile configurare tutti i parametri di connessione di Profinet. Premere il pulsante |modify| per aprire la relativa finestra di configurazione in :numref:`impostazioni_applicazione_profinet_editor_variabili`.

.. |modify| image:: _static/impostazioni_applicazione_profinet_modifica_variabili_icona.png
   :scale: 70%
   :align: middle

.. _impostazioni_applicazione_profinet_editor_variabili:
.. figure:: _static/impostazioni_applicazione_profinet_editor_variabili.png
   :width: 14 cm
   :align: center

   Finestra di Configurazione


Impostare tutti i parametri di connessione:

**CPU Type**

   Tipo di PLC Siemens (S7200, S7300, S7400, **S71200**, S71500).

**IP Address**

   Indirizzo del dispositivo nella rete.

**Rack**

   Dipende dalla configurazione del PLC, se è un singolo dispositivo 0 è il valore predefinito1.

**Slot**

   Dipende dalla configurazione del PLC, se è un singolo dispositivo 0 è il valore predefinito1.

**DataBlock**

   Indirizzo dell'area di memoria in cui sono memorizzati i dati.

**Start byte address**

   Indirizzo del primo byte di dati.

**Data Type**

   Posizione dell'area di memoria (Input, Output, Memory, **DataBlock**, Timer, Counter).

**Variable type**

   Specifica il tipo di dati (Bit, Byte, Word, DWord, Int, Dint, Real, String, **StringEx**, Timer, Counter).

**Variable count**

   Lunghezza dei dati in byte. I dati delle stringhe del modello S71200 sono di 256 byte.

Testare la lettura premendo il pulsante ``LEGGI DATI DA PLC``. Se il controllo è positivo nel campo :guilabel:`Corrente` verrà visualizzato il valore letto. Se la lettura non riesce, viene visualizzato il messaggio ``NO DATA``.

.. _impostazioni_PLC_Siemens:

##############################
CONFIGURAZIONE DEL PLC SIEMENS
##############################
************
S7 1200/1500
************
Un'apparecchiatura esterna può accedere alla CPU S71200/1500 solo tramite il protocollo S7 "base", solo lavorando come un HMI, cioè è consentito solo il trasferimento di dati di base.

Tutte le altre operazioni PG (controllo/directory/ecc...) devono seguire il protocollo esteso, non (ancora) coperto da Snap7.
In particolare per accedere a un DB nell'S71500 sono necessarie alcune impostazioni aggiuntive lato PLC.

#. È possibile accedere solo ai DB globali.
#. L'accesso al blocco ottimizzato deve essere disattivato.
#. Il livello di accesso deve essere "completo" e il "meccanismo di connessione" deve consentire GET/PUT.

Di seguito spiegazione di queste impostazioni in TIA Portal V12.

****************
PROPRIETÀ DEL DB
****************
Selezionare il DB nel riquadro a sinistra in :guilabel:`Program blocks` e premere ``Alt`` + ``Invio`` (o selezionare il menù :guilabel:`Properties`).

Deselezionare :guilabel:`Optimized block access` che, per impostazione predefinita, è selezionato (vedere :numref:`impostazioni_applicazione_profinet_proprietàDB`).

.. _impostazioni_applicazione_profinet_proprietàDB:
.. figure:: _static/impostazioni_applicazione_profinet_proprietàDB.png
   :width: 14 cm
   :align: center

   Selezione proprietà DB


**********
PROTEZIONE
**********
Selezionare :guilabel:`CPU Project` nel riquadro sinistro e e premere ``Alt`` + ``Invio`` ((o selezionare il menù :guilabel:`Properties`).
Nella finestra :guilabel:`Protection` selezionare :guilabel:`Full access` e selezionare selezionare :guilabel:`Permit access with PUT/GET ...` (vedere :numref:`impostazioni_applicazione_profinet_protezioneDB`).

.. _impostazioni_applicazione_profinet_protezioneDB:
.. figure:: _static/impostazioni_applicazione_profinet_protezioneDB.png
   :width: 14 cm
   :align: center

   Selezione proprietà Protezione

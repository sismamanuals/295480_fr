# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020, SISMA S.p.a.
# This file is distributed under the same license as the PRISMA Software
# Manuale Utente package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PRISMA Software Manuale Utente \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-01-24 10:50+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: WIP/295480/295480/source/test_focus.rst:5 a1ed9879485845199d4c9d7d70b495e0
msgid "FOCUS TEST"
msgstr ""

#: WIP/295480/295480/source/test_focus.rst:6 d8fad9c4f44444359c1fc5f39f7b5c1b
msgid ""
"È un test da eseguirsi solo nel caso in cui si abbia in sospetto che la "
"macchina laser abbia perso il suo punto di fuoco."
msgstr ""

#: 2ffd43c5c9a44153b9cacdd500914d02 WIP/295480/295480/source/test_focus.rst:8
msgid ""
"Attraverso questo test è possibile definire il nuovo punto di fuoco (o "
"confermare l'esistente)."
msgstr ""

#: WIP/295480/295480/source/test_focus.rst:10 d68419f673894d04b3277ed9eaadb423
msgid ""
"Per eseguirlo è sufficiente compilare ogni campo presente nella finestra "
"ed eseguire la marcatura su una piastra di acciaio o ottone."
msgstr ""

#: 03955750c39a44369dfaa7f69f2c1597 WIP/295480/295480/source/test_focus.rst:14
msgid "ESECUZIONE TEST"
msgstr ""

#: 7e943232b37c4dfe9ccdcc1d889356d0 WIP/295480/295480/source/test_focus.rst:15
msgid ""
"È importante sapere che il test andrà a fare una serie di marcature "
"incrociando i range di Potenza-Frequenza-Velocità."
msgstr ""

#: WIP/295480/295480/source/test_focus.rst:17 e7b1c195e1b4414ba0287f7dd2a03439
msgid "Per eseguire il test impostare:"
msgstr ""

#: 58296e1ef6f347a4b23b63dd5358f574 WIP/295480/295480/source/test_focus.rst:19
msgid ":kbd:`Altezza pezzo` (inserire l'altezza della piastrina da marcare);"
msgstr ""

#: 6290c4fdee8e4359906111c2a4fbd767 WIP/295480/295480/source/test_focus.rst:20
msgid ""
":kbd:`Altezza` (il range di altezza da campionare - attenzione che il "
"range sia adeguato alla focale che si sta testando);"
msgstr ""

#: 28ab9b0a26834181bb34069ab59a93b9 WIP/295480/295480/source/test_focus.rst:21
msgid ":kbd:`Potenza` (il range di potenza da usare);"
msgstr ""

#: WIP/295480/295480/source/test_focus.rst:22 b4d7fc52ca8b4d338a43c60047294638
msgid ":kbd:`Frequenza` (il range di frequenza da usare);"
msgstr ""

#: 603876aa8435466eac1a90dd60a0f6ab WIP/295480/295480/source/test_focus.rst:23
msgid ":kbd:`Velocità` (il range di velocità da usare);"
msgstr ""

#: WIP/295480/295480/source/test_focus.rst:24 bd4c9949830846909f24b94c25b25b8e
msgid ""
":kbd:`Spaziatura riempimento` (il valore di spaziatura scelto - si "
"consiglia di non inserire un valore troppo stringente);"
msgstr ""

#: 0184928363d24e79960751c478cb151b WIP/295480/295480/source/test_focus.rst:25
msgid ":kbd:`Linea sottile` (per utilizzare una linea sottile per la marcatura)."
msgstr ""

#: WIP/295480/295480/source/test_focus.rst:27 d60148cd969e41579c599be853876873
msgid "A questo punto si può procedere con la marcatura:"
msgstr ""

#: 2c38ad6db8714641b5a2057181d65450 WIP/295480/295480/source/test_focus.rst:29
#: WIP/295480/295480/source/test_focus.rst:59 b48097107bbc4d91b9b7b054a159b971
msgid ""
"Premere :kbd:`AVVIA ROSSO` per centrare l'area di marcatura sulla "
"piastrina."
msgstr ""

#: 1d7c8f4962e6436eaee8f7811b137ca8 WIP/295480/295480/source/test_focus.rst:30
#: WIP/295480/295480/source/test_focus.rst:60 b72bfe06467f4fc58004d2ada18acc6d
msgid "Chiudere lo sportello."
msgstr ""

#: 10e78a92287943ce80e1d94583d67851 2a64b01cb42445de8081b5d9ad0d46d4
#: WIP/295480/295480/source/test_focus.rst:31
#: WIP/295480/295480/source/test_focus.rst:61
msgid "Premere :kbd:`AVVIA LASER` per eseguire la marcatura."
msgstr ""

#: 12ac51324156483b9e9b48d1ff7bd091 WIP/295480/295480/source/test_focus.rst:38
msgid ".. image:: _static/it/focus_test.png"
msgstr ""

#: 515eeb904d994899b7c8a0ea6e923b63 WIP/295480/295480/source/test_focus.rst:38
msgid "Finestra *FOCUS TEST*"
msgstr ""

#: 4aee04f9222444539768330201411657 WIP/295480/295480/source/test_focus.rst:42
msgid "RISULTATO TEST"
msgstr ""

#: 6a17133a07784e4f988c37a66a0d59aa WIP/295480/295480/source/test_focus.rst:43
msgid ""
"Il test produrrà una serie di quadrati incisi. Alcuni quadrati potrebbero"
" essere più incisi di altri. La linea maggiormente incisa indica che in "
"quel punto il laser è maggiormente a fuoco."
msgstr ""

#: 59678cc12cf34cdcbc5ce2bc3f36a430 WIP/295480/295480/source/test_focus.rst:46
msgid ""
"Per indicare al sistema che il punto di fuoco è quello che vediamo sulla "
"piastrina è sufficiente spostare il cursore sul numero corrispondente "
"alla linea di quadratini identificata. Fatto questo è necessario premere "
"su :kbd:`APPLICA` per inserire il nuovo valore trovato."
msgstr ""

#: 77d2bfbc99c44d65bfc0270bbd5f1b07 WIP/295480/295480/source/test_focus.rst:51
msgid ""
"|notice| È possibile che le linee maggiormente marcate siano 2 e siano "
"contigue. In quella situazione è possibile selezionare anche un valore "
"intermedio (ad esempio, tra 3 e 4)."
msgstr ""

#: 7b9a653761704366a535e5fe74aeb329 <rst_prolog>:14
msgid ""
".. image:: _static/notice.png\n"
"   :alt: notice"
msgstr ""

#: 292933ce24fa4c17befe96b1b14e438c WIP/295480/295480/source/test_focus.rst:53
msgid ""
"*Se il range di altezza selezionato è molto ampio potrebbe essere "
"necessario stringere il range intorno ad un valore identificato nel primo"
" test focus. Per farlo è sufficiente spostare il cursore sopra il numero "
"della linea attorno cui vogliamo impostare il nuovo range di altezza e "
"premere :kbd:`CONTINUA`.* *da spiegare*"
msgstr ""

#: WIP/295480/295480/source/test_focus.rst:56 b30b461aab0347c1be1bb3b4dc851680
msgid ""
"A questo punto il sistema imposterà in automatico i nuovi valori dell' "
"Altezza. Sarà sufficiente procedere con la marcatura:"
msgstr ""

#: 117ee79b7cd64f63a7dbbbacecddba67 WIP/295480/295480/source/test_focus.rst:63
msgid ""
"Una volta eseguito il secondo test si può identificare la/e linea/e di "
"quadratini maggiormente marcati e spostare il cursore sul numero "
"corrispondente alla linea. Per salvare il nuovo valore di fuoco si deve "
"premere :kbd:`APPLICA`."
msgstr ""

#: 66df623858234aeba69eca8577f2924a WIP/295480/295480/source/test_focus.rst:66
msgid ""
"A questo punto è possibile chiudere il Test Focus e rifare l'Homing della"
" macchina prima di procedere con qualsiasi altra attività di marcatura."
msgstr ""

#~ msgid ".. image:: _static/Immagine_it_focus_test.png"
#~ msgstr ""

